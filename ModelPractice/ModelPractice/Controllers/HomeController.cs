﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ModelPractice.Models;

namespace ModelPractice.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            TestModel model = new TestModel()
            {
                ID = 1,
                FirstName = "James",
                LastName = "Beck",
            };

            return View(model);
        }

        public IActionResult Names()
        {
            TestModel model = new TestModel()
            {
                ID = 1,
                FirstName = "James",
                LastName = "Beck",
            };

            return View(model);
        }

        public string TestString()
        {
            return "This is a test";
        }
    }
}
